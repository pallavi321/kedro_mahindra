"""Project pipelines."""
from typing import Dict
from kedro.pipeline import Pipeline, pipeline
from .pipelines import data_engineering, data_science


def register_pipelines() -> Dict[str, Pipeline]:
    """Register the project's pipelines.

    Returns:
        A mapping from a pipeline name to a ``Pipeline`` object.
    """
    data_engineering_pipeline = data_engineering.create_pipeline()
    data_science_pipeline = data_science.create_pipeline()

    return { "de": data_engineering_pipeline,
        "ds": data_science_pipeline,
        "__default__": data_engineering_pipeline + data_science_pipeline,}



