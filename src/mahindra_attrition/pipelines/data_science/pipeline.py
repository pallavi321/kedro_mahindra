"""
This is a boilerplate pipeline 'data_science'
generated using Kedro 0.18.1
"""

from kedro.pipeline import Pipeline, node, pipeline
from kedro.pipeline.modular_pipeline import pipeline
from .nodes import split_data, oversampling, model_train,model_health


def create_pipeline(**kwargs) -> Pipeline:
    return pipeline([
        node(
            func=split_data,
            inputs=["transformed_data", "params:split_data"],
            outputs=["X", "y", "X_train", "X_test", "y_train", "y_test"],
            name="split_data_node",
        ),
        node(
            func=oversampling,
            inputs=["X_train", "y_train"],
            outputs=["X_res", "y_res"],
            name="oversampling_node",
        ),
        node(
            func=model_train,
            inputs=["X_res", "y_res", "params:model_train"],
            outputs="rfr",
            name="model_train_node",
        ),
        node(
            func=model_health,
            inputs=["rfr", "transformed_data",  "X_train", "X_test", "y_train", "y_test"],
            outputs="model_health_output_dict",
            name="model_health_node",
        ),
    ],
        # namespace="data_science",
        # inputs="transformed_data",
        # outputs="transformed_data",
    )
