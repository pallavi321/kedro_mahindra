"""
This is a boilerplate pipeline 'data_science'
generated using Kedro 0.18.1
"""
from typing import Any, Dict, Tuple
import pickle
from collections import Counter
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from imblearn.over_sampling import RandomOverSampler
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.metrics import confusion_matrix,roc_auc_score, accuracy_score,recall_score,precision_score,\
f1_score,classification_report,roc_curve, auc
from sklearn import metrics


def split_data(data: pd.DataFrame, parameters: Dict) -> Tuple:
    """Splits data into features and targets training and test sets.

     Args:
         data: Data containing features and target.
         parameters: Parameters defined in parameters/data_science.yml.
     Returns:
         Split data.
     """
    try:
#         if target_col in final_data.columns:
        X = data.drop(labels=['employee_id',parameters["features"]],axis=1)
        y = data[parameters["features"]]
        X_train,X_test,y_train,y_test = train_test_split(X,y,random_state=101)
        return X,y, X_train,X_test,y_train,y_test
    except Exception as e:
        print (e)


def oversampling(data, label):
    ros = RandomOverSampler(random_state=42)
    X_res, y_res = ros.fit_resample(data, label)
    # print("..............X_res",X_res)
    # print("..............y_res",y_res)
    return X_res, y_res


def model_train(data, label,  parameters: Dict):
    """Train model .

     Args:
         data: X_res
         label: y_res
         parameters: Parameters defined in parameters/data_science.yml.
     Returns:
         Split data.
     """
    rfr = RandomForestClassifier(n_estimators=parameters["n_estimators"],
                                 max_depth=parameters["max_depth"],
                                 min_samples_split=parameters["min_samples_split"],
                                 max_features=parameters["max_features"],
                                 min_impurity_decrease=parameters["min_impurity_decrease"])
    rfr.fit(data, label)
    # print("...........rfr",rfr)
    return rfr


def model_health(model: RandomForestClassifier, data: pd.DataFrame, X_train: pd.DataFrame , X_test: pd.DataFrame,
                 y_train: pd.Series, y_test: pd.Series):
    try:
        output_dict = dict()
        all_quarters = []
        # X, y, X_train, X_test, y_train, y_test = split_data(data, parameters["features"])
        y_train_pred = model.predict(X_train)
        y_test_pred = model.predict(X_test)
        output_dict['training_rows'] = X_train.shape[0]
        output_dict['testing_rows'] = X_test.shape[0]
        output_dict['train_accuracy'] = round(accuracy_score(y_train, y_train_pred), 4)
        output_dict['test_accuracy'] = round(accuracy_score(y_test, y_test_pred), 4)
        output_dict['train_precision'] = round(precision_score(y_train, y_train_pred, average='macro'), 4)
        output_dict['test_precision'] = round(precision_score(y_test, y_test_pred, average='macro'), 4)
        output_dict['train_recall'] = round(recall_score(y_train, y_train_pred, average='macro'), 4)
        output_dict['test_recall'] = round(recall_score(y_test, y_test_pred, average='macro'), 4)
        output_dict['train_f1'] = round(f1_score(y_train, y_train_pred, average='macro'), 4)
        output_dict['test_f1'] = round(f1_score(y_test, y_test_pred, average='macro'), 4)
        output_dict['auc_test'] = round(recall_score(y_test, y_test_pred, average='macro'), 4)
        output_dict['auc_train'] = round(recall_score(y_train, y_train_pred, average='macro'), 4)
    except Exception as e:
        print("error in model health",e)
        output_dict = dict()
    # print("............output_dict",output_dict)
    return output_dict

