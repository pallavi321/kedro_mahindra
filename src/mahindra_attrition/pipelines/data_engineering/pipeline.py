"""
This is a boilerplate pipeline 'data_engineering'
generated using Kedro 0.18.1
"""

from kedro.pipeline import Pipeline, node, pipeline
from kedro.pipeline.modular_pipeline import pipeline
from .nodes import (cleaning_func, col_slection, data_manip_simul_train)


def create_pipeline(**kwargs) -> Pipeline:
    return pipeline(
        [
            node(
                func=cleaning_func,
                inputs=["prediction_data_for_mf", "prediction_data_for_mf_test_db"],
                outputs="cleaned_dataframe",
                name="cleaning_func_node",
            ),
            node(
                func=col_slection,
                inputs="cleaned_dataframe",
                outputs="df_having_selected_col",
                name="col_slection_node",
            ),
            node(
                func=data_manip_simul_train,
                inputs="df_having_selected_col",
                outputs= ["transformed_data", "prediction_data_for_mf_test_db_with_table"],
                name="data_manip_simul_train_node",
            ),
        ],
            namespace="data_engineering",
            inputs=["prediction_data_for_mf", "prediction_data_for_mf_test_db"],
            outputs=["transformed_data", "prediction_data_for_mf_test_db_with_table"],
    )

